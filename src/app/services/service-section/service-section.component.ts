import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'app-service-section',
  inputs:['title','imagePath'],
  templateUrl: './service-section.component.html',
  styleUrls: ['./service-section.component.css']
})
export class ServiceSectionComponent implements OnInit {
  
  constructor() { }

  @Input() title = '';
  @Input() imagePath = '';

  ngOnInit(): void {
  }

}
